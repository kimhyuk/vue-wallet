import Send from "@/components/dashboard/container/Send.vue";
import Dashboard from "@/views/Dashboard.vue";


export default {
    path: '/dashboard',
    name: 'dashboard',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Dashboard,
    children: [
        // UserHome will be rendered inside User's <router-view>
        // when /user/:id is matched
        { path: "", redirect:"send"},
        { path: 'send', name: "send", component: Send},
    ]
}