import { WEB3 } from "@/constants/store";

const { ACTIONS, MUTATIONS } = WEB3;




// initial state
const state = {
  web3: null,
  coinbase: "",
  networkId: null,
  isListening: false,
  contract: null,
  status: "",
  balance: "0",

}
// getters
const getters = {

}

// actions
const actions = {
  [ACTIONS.INIT] ({commit}, payload){
    commit(MUTATIONS.INIT, payload);
  },
  [ACTIONS.SET_WEB3] ({commit}, payload){
    commit(MUTATIONS.SET_WEB3, payload);
  },
  [ACTIONS.SET_STATUS] ({commit}, payload) {
    commit(MUTATIONS.SET_STATUS, payload);
  },
  [ACTIONS.SET_CONTRACT] ({commit}, payload) {
    commit(MUTATIONS.SET_CONTRACT, payload);
  },
  async [ACTIONS.REFRESH_BALANCE] ({state,commit}){
    try {
      const balance = await state.web3.eth.getBalance(state.coinbase);
      commit(MUTATIONS.REFRESH_BALANCE, balance);

      return new Promise(r=>r());
    } catch(err) {
      throw err;
    }
  },
  async [ACTIONS.SEND_COIN] ({state, commit}, payload) {
    try {
      const { web3, coinbase } = state;
      console.log(web3,coinbase);
      const hash = await web3.eth.sendTransaction({
        from: coinbase,
        to: payload.to,
        value:  web3.utils.toWei(String(payload.value), "ether"),
        gasPrice: payload.gasPrice || web3.eth.defaultGasPrice,
        gas: payload.gas || web3.eth.defaultGas,
      })

      const balance = await state.web3.eth.getBalance(state.coinbase);
      commit(MUTATIONS.REFRESH_BALANCE,balance);

      return new Promise((resolve)=>resolve(hash));      
    } catch (err) {
      throw err;
    }
  }
}
// mutations
const mutations = {
  [MUTATIONS.INIT] (state, payload){
    state.web3 = payload.web3;
    state.coinbase = payload.coinbase;
    state.networkId = payload.networkId;
    state.isListening = payload.isListening;
  },

  [MUTATIONS.SET_WEB3] (state, payload){
    state.web3 = payload;
  },

  [MUTATIONS.SET_STATUS] (state, payload) {
    state.status = payload;
  },

  [MUTATIONS.REFRESH_BALANCE] (state, payload){
    state.balance = state.web3.utils.fromWei(String(payload),"ether");
  },

  [MUTATIONS.SET_CONTRACT] (state, payload) {
    state.contract = payload;
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}