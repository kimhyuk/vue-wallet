import user from "./user";
import web3 from "./web3";
import dashboard from "./dashbard";

export default {
    user,
    web3,
    dashboard
}