import { DASHBOARD } from "../../constants/store";

const { ACTIONS,MUTATIONS } = DASHBOARD;

// initial state
const state = {
  drawerVisible: true,
  snackbar: {
    visible: false,
    message: ""
  },
  dialogs: []
}

// getters
const getters = {

}

// actions
const actions = {
    [ACTIONS.DRAWER_VISIBLE_CHANGE]({ commit }) {
      commit(ACTIONS.DRAWER_VISIBLE_CHANGE);
    },
    [ACTIONS.SET_ON_SNACKBAR] ({ commit },options) {
      commit(ACTIONS.SET_ON_SNACKBAR, options);
    }
}

// mutations
const mutations = {
  [MUTATIONS.DRAWER_VISIBLE_CHANGE] (state) {
    state.drawerVisible = !state.drawerVisible;
  },
  [MUTATIONS.SET_ON_SNACKBAR] (state,options) {
    state.snackbar.visible = options.visible;
    state.snackbar.message = options.message;
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}