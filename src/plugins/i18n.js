import VeeValidate from 'vee-validate';
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import enValidation from 'vee-validate/dist/locale/en';
import koValidation from "vee-validate/dist/locale/ko";

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: "ko",
    fallbackLocale:"ko"
});

Vue.use(VeeValidate, {
   i18nRootKey: 'validations', // customize the root path for validation messages.
   i18n,
   dictionary: {
    en: enValidation,
    ko: koValidation
   }
});