
// dashboard const

let dashboard = {};

dashboard.ACTIONS = {
    DRAWER_VISIBLE_CHANGE: "drawerVisibleChange",
    SET_ON_SNACKBAR: "setOnSnackBar"
}

dashboard.MUTATIONS = {
    ...dashboard.ACTIONS
}


export const DASHBOARD = dashboard;


// web3 const

let web3 = {};
const WEB3_COMMON = {
    SET_WEB3: "setWeb3",
    SET_STATUS: "setStatus",
    SET_CONTRACT: "setContract",
    REFRESH_BALANCE: "refreshBalance",
    INIT: "init",
}

web3.ACTIONS = {
    ...WEB3_COMMON,
    SEND_COIN: "sendCoin",
}

web3.MUTATIONS = {
    ...WEB3_COMMON
}


export const WEB3 = web3;