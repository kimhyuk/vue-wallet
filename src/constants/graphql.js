import gql from 'graphql-tag'

// 2
export const ALL_USERS_QUERY = gql`
  query users {
    users {
        _id
        email
        username
    }
  }
`

export const CREATE_USER_MUTATION = gql`
  # 2
  mutation signup($email: String!, $username: String!,$password: String!) {
    signup(
      email: $email,
      username: $username,
      password: $password
    )
  }
`

export const LOGIN_USER_MUTATION = gql`
  mutation login($email: String!, $password: String!) {
    login(
      email:$email,
      password:$password
    )
  }
`