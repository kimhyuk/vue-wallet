import Web3 from 'web3'

export default new Promise((resolve,reject) => {
  window.addEventListener("load", async function(){
    let web3 = window.web3;
    try {
      if(typeof web3 == "undefined" ) {
        // Use Mist/MetaMask's provider
        throw new Error("not metamask");
      }

      web3 = new Web3(web3.currentProvider)
      const [ isListening, networkId, coinbase ] = await Promise.all([
        web3.eth.net.isListening(),
        web3.eth.net.getId(),
        web3.eth.getCoinbase()
      ]);
      let address = web3.eth.defaultAccount;

      let app = {
        web3,
        isListening,
        networkId,
        coinbase,
        address
      }

      resolve(app);
    } catch(err) {
      console.log("error: " + err);
      reject(err);
    }
  });
});

  
